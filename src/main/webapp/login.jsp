<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>抓取系统后台管理</title>
<script type="text/javascript">
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>
</head>
<body>
<form action="<c:url value="/admin/login"/>" method="post">
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">登录</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>用户名</td>
			<td>
				<input type="text" class="InputCommon" name="username" />
			</td>
			<td>默认：admin</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>密　码</td>
			<td>
				<input type="password" class="InputCommon" name="password" />
			</td>
			<td>默认：admin</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td colspan="2">
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
		</tr>
	</table>
</form>
</body>
</html>