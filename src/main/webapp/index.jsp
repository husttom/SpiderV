<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>抓取系统管理首页</title>
<script type="text/javascript">
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>
</head>
<body>
<%
	Object obj=request.getSession().getAttribute("username");
	if(obj == null){
		response.sendRedirect("login.jsp");
	}
%>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">系统配置</a></li>
		<li><a href="#tabs-2">系统运行</a></li>
		<li><a href="#tabs-3">运行统计</a></li>
		<li><a href="#tabs-4">系统清理</a></li>
		<li><a href="#tabs-5">辅助工具</a></li>
	</ul>
	<div id="tabs-1">
		<ul>
			<li>
				<a href="<c:url value="/admin/engine/form"/>">添加搜索引擎</a>
			</li>
			<li>
				<a href="<c:url value="/admin/engine"/>">搜索引擎列表</a>
			</li>
			<li>
				--------------------------------
			</li>
			<li>
				<a href="<c:url value="/admin/param/form"/>">添加搜索引擎参数</a>
			</li>
			<li>
				<a href="<c:url value="/admin/param/engine/1"/>">搜索引擎参数列表</a>
			</li>
			<li>
				--------------------------------
			</li>
			<li>
				<a href="<c:url value="/admin/listpage/form"/>">添加列表页面解析配置</a>
			</li>
			<li>
				<a href="<c:url value="/admin/listpage"/>">列表页面解析配置列表</a>
			</li>
			<li>
				--------------------------------
			</li>
			<li>
				<a href="<c:url value="/admin/task/form"/>">添加本机自动运行计划</a>
			</li>
			<li>
				<a href="<c:url value="/admin/task"/>">集群自动运行计划列表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/task/local"/>">本机自动运行计划列表</a>
			</li>
		</ul>
	</div>
	<div id="tabs-2">
		<ul>
			<li>
				<a href="<c:url value="/admin/createurl/form"/>">组合参数生成URL,写入QUERY表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/queryurl/form/initValid"/>" >从QUERY_BAK表导入数据到QUERY表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/createurl/formBack"/>">QUERY表导出到QUERY_BAK表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/createurl/insert"/>">手动添加URL导出到QUERY_BAK表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/queryurl/form/deleteValid"/>" >删除QUERY_BAK的备份数据</a>
			</li>
			<li>
				<a href="<c:url value="/admin/spider/form"/>">执行抓取</a>
			</li>
		</ul>
	</div>
	<div id="tabs-3">
		<ul>
			<li>
				<a href="<c:url value="/admin/statis"/>">当前系统运行信息（速度，处理URL量，抓取记录数量）</a>
			</li>
			<li>
				<a href="<c:url value="/admin/log/form"/>">历史运行记录</a>
			</li>
			<li>
				<a href="<c:url value="/admin/spider/view_tables"/>">查看抓取记录保存表前10条记录</a>
			</li>
		</ul>
	</div>
	<div id="tabs-4">
		<ul>
			<li>
				<a href="<c:url value="/admin/queryurl/form/truncate"/>" >删除搜索URL运行表数据(truncate表)</a>
			</li>
			<li>
				<a href="<c:url value="/admin/spider/drop"/>">删除抓取记录保存表(网站+日期drop表)</a>
			</li>
		</ul>
	</div>
	<div id="tabs-5">
		<ul>
			<li>
				<a href="<c:url value="/admin/spider/test"/>">抓取测试（当某个网站抓取出现异常时，可迅速定位问题原因）</a>
			</li>
			<li>
				<a href="<c:url value="/admin/tools/form2"/>">抓取HTML工具（给定一个URL获取HTML代码，用于排查问题）</a>
			</li>
			<li>
				<a href="<c:url value="/admin/etl/clear_websites"/>">重新加载网站列表</a>
			</li>
			<li>
				<a href="<c:url value="/admin/engine/export"/>">导出搜索引擎配置</a>
			</li>
			<li>
				<a href="<c:url value="/admin/engine/import"/>">导入搜索引擎配置</a>
			</li>
		</ul>
	</div>
</div>
<a href="<c:url value="/admin/logout"/>">退出系统</a>
</body>
</html>