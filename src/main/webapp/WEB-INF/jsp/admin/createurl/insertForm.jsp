<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>手动插入搜索URL到备份表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/createurl/insert"/>" method="post">
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">手动插入搜索URL到备份表</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<option value="${engine.id}">${engine.name}</option>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">URL列表</td>
			<td>
				<textarea rows="15" cols="80" name="urlStrs"></textarea>
			</td>
			<td>用换行符分割</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
