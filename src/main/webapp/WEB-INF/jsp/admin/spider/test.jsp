<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/js/jquery-1.4.2.min.js"/>"></script>
<title>测试抓取流程</title>
<script type="text/javascript">
	$(document).ready(function(){
		$("#searchEngineId").bind("change", function(){
			var selectEngineId=$("#searchEngineId").val();
			$("textarea[name='postUrl']").text("加载中，请稍等！");
			$.get("<c:url value="/admin/queryurl/firstValidPostUrl/"/>"+selectEngineId, function(result){
				$("textarea[name='postUrl']").text(result);
			  });
		}); 
	}); 
	function setPostUrl(a){
		var href=$(a).attr("href");
		$("textarea[name='postUrl']").text(href);
	}
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/spider/test"/>" method="post">
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">测试抓取流程</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId" id="searchEngineId" >
					<c:forEach var="engine" items="${engines}">
						<c:choose>
							<c:when test="${searchEngineId == engine.id}">
								<option value="${engine.id}" selected="selected" >${engine.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${engine.id}">${engine.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">测试POST_URL:</td>
			<td>
				<textarea class="InputCommon" name="postUrl" rows="15" cols="80">${postUrl}</textarea>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">测试URL:</td>
			<td>
				&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="测试"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<div id="result">
	<table>
		<tr>
			<td colspan="2" class="tableHeadBg">
				<div align="center">测试结果信息</div>
			</td>
		</tr>
		<tr class="tableTitleBg">
			<td width="20%">处理过程描述</td>
			<td width="80%">信息</td>
		</tr>
		<c:forEach var="testMsg" items="${testMsgs}" varStatus="status">
			<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
				<td>${testMsg[0]}</td>
				<td>${testMsg[1]}</td>
			</tr>
		</c:forEach>
	</table>
</div>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
<ul>
	<li>
		测试抓取流程
	</li>
</ul>
</body>
</html>
