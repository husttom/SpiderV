package com.yzq.os.spider.v.service.addition;

import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 抽象登陆处理类
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public abstract class AbstractLogin implements Login {

	/**
	 * 网络访问服务类
	 */
	protected HttpClientService httpClientService;

	@Override
	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

}
