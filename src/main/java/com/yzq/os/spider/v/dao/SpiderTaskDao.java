package com.yzq.os.spider.v.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.dao.mapper.SpiderTaskMapper;
import com.yzq.os.spider.v.domain.SpiderTask;

@Repository
public class SpiderTaskDao extends AbstractDao {
	public int save(final SpiderTask task) {
		final StringBuffer sql = new StringBuffer();
		sql.append(" insert into spider_task ");
		sql.append("   (run_server_ip, ");
		sql.append("    search_engine_id, ");
		sql.append("    find_url_size, ");
		sql.append("    min_thread_num, ");
		sql.append("    max_thread_num, ");
		sql.append("    cron_exp,");
		sql.append("    pool_queue_size) ");
		sql.append(" values ");
		sql.append("   (?, ?, ?, ?, ?, ?, ?) ");

		KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplateOnline.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql.toString(), new String[] { "id" });
				ps.setString(1, task.getRunServerIp());
				ps.setInt(2, task.getSearchEngineId());
				ps.setInt(3, task.getFindUrlSize());
				ps.setInt(4, task.getMinThreadNum());
				ps.setInt(5, task.getMaxThreadNum());
				ps.setString(6, task.getCronExp());
				ps.setInt(7, task.getPoolQueueSize());
				return ps;
			}
		}, keyHolder);

		return keyHolder.getKey().intValue();
	}

	public List<SpiderTask> findLocalTasks(List<String> localIps) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select ct.id, ");
		sql.append("        ct.run_server_ip, ");
		sql.append("        ct.search_engine_id, ");
		sql.append("        se.name as search_engine_name, ");
		sql.append("        ct.find_url_size, ");
		sql.append("        ct.min_thread_num, ");
		sql.append("        ct.max_thread_num, ");
		sql.append("        ct.pool_queue_size, ");
		sql.append("        ct.cron_exp ");
		sql.append("   from spider_task ct, search_engine se ");
		sql.append("  where ct.search_engine_id = se.id ");
		sql.append("    and ct.run_server_ip = ? ");
		sql.append("  order by search_engine_name ");

		List<SpiderTask> allTasks = new ArrayList<SpiderTask>();
		for (String localIp : localIps) {
			List<SpiderTask> tasks = jdbcTemplateOnline.query(sql.toString(), new Object[] { localIp }, new SpiderTaskMapper());
			allTasks.addAll(tasks);
		}
		return allTasks;
	}

	public void update(SpiderTask task) {
		StringBuffer sql = new StringBuffer();
		sql.append(" update spider_task ");
		sql.append("    set run_server_ip    = ?, ");
		sql.append("        search_engine_id = ?, ");
		sql.append("        find_url_size    = ?, ");
		sql.append("        min_thread_num   = ?, ");
		sql.append("        max_thread_num   = ?, ");
		sql.append("        cron_exp         = ?, ");
		sql.append("        pool_queue_size  = ? ");
		sql.append("  where id = ? ");

		List<Object> params = new ArrayList<Object>();
		params.add(task.getRunServerIp());
		params.add(task.getSearchEngineId());
		params.add(task.getFindUrlSize());
		params.add(task.getMinThreadNum());
		params.add(task.getMaxThreadNum());
		params.add(task.getCronExp());
		params.add(task.getPoolQueueSize());
		params.add(task.getId());

		jdbcTemplateOnline.update(sql.toString(), params.toArray());
	}

	public List<SpiderTask> find() {
		StringBuffer sql = new StringBuffer();
		sql.append(" select ct.id, ");
		sql.append("        ct.run_server_ip, ");
		sql.append("        ct.search_engine_id, ");
		sql.append("        se.name as search_engine_name, ");
		sql.append("        ct.find_url_size, ");
		sql.append("        ct.min_thread_num, ");
		sql.append("        ct.max_thread_num, ");
		sql.append("        ct.pool_queue_size, ");
		sql.append("        ct.cron_exp ");
		sql.append("   from spider_task ct, search_engine se ");
		sql.append("  where ct.search_engine_id = se.id ");
		sql.append("  order by search_engine_name ");

		return jdbcTemplateOnline.query(sql.toString(), new SpiderTaskMapper());
	}

	public SpiderTask load(int id) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select id, ");
		sql.append("        run_server_ip, ");
		sql.append("        search_engine_id, ");
		sql.append("        find_url_size, ");
		sql.append("        min_thread_num, ");
		sql.append("        max_thread_num, ");
		sql.append("        pool_queue_size, ");
		sql.append("        cron_exp ");
		sql.append("   from spider_task ");
		sql.append("  where id = ? ");

		return jdbcTemplateOnline.queryForObject(sql.toString(), new Object[] { id }, new SpiderTaskMapper());
	}

	public void delete(int id) {
		String sql = " delete from spider_task where id = ? ";
		jdbcTemplateOnline.update(sql, id);
	}

}
