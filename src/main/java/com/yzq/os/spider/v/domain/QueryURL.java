package com.yzq.os.spider.v.domain;

/**
 * 搜索URL，用来提交进行抓取
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class QueryURL {
	/**
	 * 等待抓取
	 */
	public static final int WAIT = 0;
	/**
	 * 正在抓取
	 */
	public static final int DOING = 1;

	/**
	 * 抓取完毕
	 */
	public static final int OK = 2;
	/**
	 * 由于网站有最大返回数量限制，系统自动添加搜索条件减少结果集数量，但是没找到能添加的条件
	 */
	public static final int ERROR_NO_REQUIRED_MULTIPLE_VALUE_PARAM = 301;

	/**
	 * 由于网站有最大返回数量限制，系统自动添加搜索条件减少结果集数量，但是已经添加了所有能添加的条件
	 */
	public static final int ERROR_NO_NEXT_QUALIFICATION_PARAM = 302;
	/**
	 * 网络访问异常
	 */
	public static final int ERROR_NETWORK_ACCESS = 303;
	/**
	 * HTML页面代码为空
	 */
	public static final int ERROR_HTML_PAGE_SOURCE_BLANK = 304;

	/**
	 * “没有结果”页面
	 */
	public static final int ERROR_NO_FOUNDED_JOB_PAGE = 305;

	/**
	 * 列表记录部分HTML代码为空
	 */
	public static final int ERROR_RECORD_LIST_HTML_SOURCE_BLANK = 306;

	/**
	 * 抽取到的列表记录数量为空
	 */
	public static final int ERROR_RECORD_LIST_IS_EMPTY = 307;

	/**
	 * 其它未知错误
	 */
	public static final int ERROR_OTHER = 3;

	private int id;

	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;

	/**
	 * 拼接URL，所有请求无论GET或POST,参数首先均以GET方式拼接并保存到这个属性中。
	 */
	private String spellUrl;

	/**
	 * 真正需要提交的URL
	 */
	private String postUrl;

	/**
	 * 处理状态
	 */
	private int doFlag;

	public QueryURL() {
	}

	public QueryURL(int searchEngineId, String spellUrl, String postUrl) {
		super();
		this.searchEngineId = searchEngineId;
		this.spellUrl = spellUrl;
		this.postUrl = postUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public String getSpellUrl() {
		return spellUrl;
	}

	public void setSpellUrl(String spellUrl) {
		this.spellUrl = spellUrl;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public int getDoFlag() {
		return doFlag;
	}

	public void setDoFlag(int doFlag) {
		this.doFlag = doFlag;
	}

	@Override
	public String toString() {
		return "QueryURL [id=" + id + ", searchEngineId=" + searchEngineId
				+ ", spellUrl=" + spellUrl + ", postUrl=" + postUrl
				+ ", doFlag=" + doFlag + "]";
	}

}
