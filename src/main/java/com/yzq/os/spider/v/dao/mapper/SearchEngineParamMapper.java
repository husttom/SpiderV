package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.SearchEngineParam;

public class SearchEngineParamMapper implements RowMapper<SearchEngineParam> {

	@Override
	public SearchEngineParam mapRow(ResultSet rs, int index) throws SQLException {
		SearchEngineParam param = new SearchEngineParam();
		param.setId(rs.getInt("id"));
		param.setSearchEngineId(rs.getInt("search_engine_id"));
		param.setName(rs.getString("name"));
		param.setValue(rs.getString("value"));
		param.setSingleValue(rs.getInt("single_value"));
		param.setRequired(rs.getInt("required"));
		param.setOrderNum(rs.getInt("order_num"));
		param.setDesc(rs.getString("describe_txt"));
		return param;
	}

}
