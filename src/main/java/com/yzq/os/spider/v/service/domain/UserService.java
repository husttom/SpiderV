package com.yzq.os.spider.v.service.domain;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 用户服务类
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Service
public class UserService {

	private static Logger logger = Logger.getLogger(UserService.class);

	private static final String SESSION_USER_KEY = "username";

	@Value("${app.login.username}")
	private String loginUsername;

	@Value("${app.login.password}")
	private String loginPassword;

	/**
	 * 用户登陆
	 * 
	 * @param username
	 * @param password
	 * @param session
	 * @return
	 */
	public boolean login(String username, String password, HttpSession session) {
		boolean result = StringUtils.equalsIgnoreCase(loginUsername, username)
				&& StringUtils.equalsIgnoreCase(loginPassword, password);
		if (result) {
			session.setAttribute(SESSION_USER_KEY, username);
			logger.info("User[" + username + "] login success!");
		} else {
			logger.info("User[" + username + "],Password[" + password
					+ "] login failed!");
		}
		return result;
	}

	/**
	 * 用户退出
	 * 
	 * @param session
	 */
	public void logout(HttpSession session) {
		Object obj = session.getAttribute(SESSION_USER_KEY);
		if (obj != null) {
			String username = (String) obj;
			session.removeAttribute(SESSION_USER_KEY);
			logger.info("User[" + username + "] logout success!");
		}
	}

}
