package com.yzq.os.spider.v.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.yzq.os.spider.v.domain.ListPageConfig;
import com.yzq.os.spider.v.service.domain.ListPageConfigService;
import com.yzq.os.spider.v.service.domain.SearchEngineService;

/**
 * 搜索列表页配置
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Controller
@RequestMapping("/listpage")
public class ListPageConfigController extends CommonController {

	@Autowired
	private SearchEngineService searchEngineService;

	@Autowired
	private ListPageConfigService listPageConfigService;

	/**
	 * 通过搜索引擎ID获取列表配置
	 * 
	 * @param eId
	 * @return
	 */
	@RequestMapping("/formByEngineId/{eId}")
	public ModelAndView formByEngineId(@PathVariable int eId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		ListPageConfig listPageConfig = new ListPageConfig();
		listPageConfig.setSearchEngineId(eId);
		model.put("listPageConfig", listPageConfig);
		return new ModelAndView("/admin/listpage/form", model);
	}

	/**
	 * 添加列表配置
	 * 
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView form() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		return new ModelAndView("/admin/listpage/form", model);
	}

	/**
	 * 保存列表配置
	 * 
	 * @param lpc
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public RedirectView save(ListPageConfig lpc) {
		if (lpc.getId() == null) {
			if (!listPageConfigService.isExist(lpc.getSearchEngineId())) {
				listPageConfigService.save(lpc);
			}
		} else {
			listPageConfigService.update(lpc);
		}
		return new RedirectView("listpage/form");
	}

	/**
	 * 列表配置的列表
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView list() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("listPageConfigs", listPageConfigService.findAll());
		return new ModelAndView("/admin/listpage/list", model);
	}

	/**
	 * 通过搜索引擎ID修改列表配置
	 * 
	 * @param eId
	 * @return
	 */
	@RequestMapping("/modifyByEngineId/{eId}")
	public ModelAndView modifyByEngineId(@PathVariable int eId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		model.put("listPageConfig",
				listPageConfigService.findBySearchEngineId(eId));
		return new ModelAndView("/admin/listpage/form", model);
	}

	/**
	 * 通过ID修改列表配置
	 * @param lpcId
	 * @return
	 */
	@RequestMapping("/modify/{lpcId}")
	public ModelAndView modifyById(@PathVariable int lpcId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		model.put("listPageConfig",
				listPageConfigService.findById(lpcId));
		return new ModelAndView("/admin/listpage/form", model);
	}

}
