package com.yzq.os.spider.v.service.queryurl.placeholder.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import com.yzq.os.spider.v.service.queryurl.placeholder.PlaceHolder;

/**
 * 占位符号替换抽象类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public abstract class AbstractPlaceHolder implements PlaceHolder {
	protected Logger logger = Logger.getLogger(getClass());

	protected String placeHolderString;

	protected String urlEncode;

	@Override
	public void setUrlEncode(String urlEncode) {
		this.urlEncode = urlEncode;
	}

	protected String getEncodedHolderString() {
		try {
			return URLEncoder.encode(placeHolderString, urlEncode);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
		}
		return "";
	}

	protected String getEncodedHolderValue(String value) {
		try {
			return URLEncoder.encode(value, urlEncode);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
		}
		return "";
	}

}
