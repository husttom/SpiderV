package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.ListPageConfig;

public class ListPageConfigMapper implements RowMapper<ListPageConfig> {

	@Override
	public ListPageConfig mapRow(ResultSet rs, int index) throws SQLException {
		ListPageConfig config = new ListPageConfig();
		config.setId(rs.getInt("id"));
		try {
			config.setWebsiteId(rs.getInt("website_id"));
		} catch (Exception e) {
		}
		config.setSearchEngineId(rs.getInt("search_engine_id"));
		try {
			config.setSearchEngineName(rs.getString("search_engine_name"));
		} catch (Exception e) {
		}
		config.setNoDataPageRegex(rs.getString("no_data_page_regex"));
		config.setReturnRecordNumRegex(rs.getString("return_record_num_regex"));
		config.setCurrentPageNoName(rs.getString("current_page_no_name"));
		config.setPageSize(rs.getInt("page_size"));
		config.setMaxRecordNum(rs.getInt("max_record_num"));
		config.setDataRegionRegex(rs.getString("data_region_regex"));
		config.setJobTitleRegex(rs.getString("job_title_regex"));
		config.setJobHrefRegex(rs.getString("job_href_regex"));
		config.setJobDateRegex(rs.getString("job_date_regex"));
		config.setJobDatePattern(rs.getString("job_date_pattern"));
		config.setJobCityRegex(rs.getString("job_city_regex"));
		config.setJobTypeName(rs.getString("job_type_name"));
		config.setIndustryName(rs.getString("job_industry_name"));
		config.setCityName(rs.getString("job_city_name"));
		config.setCompanyNameRegex(rs.getString("company_name_regex"));
		config.setCompanyHrefRegex(rs.getString("company_href_regex"));
		return config;
	}

}
