package com.yzq.os.spider.v.util;

/**
 * 全角半角转换工具类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public final class QuanBanTransform {

	private QuanBanTransform(){}
	
	/**
	 * 全角字符串转换半角字符串
	 * @param input
	 * @return
	 */
	public static String quan2ban(String input) {
		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '\u3000') {
				c[i] = ' ';
			} else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
				c[i] = (char) (c[i] - 65248);

			}
		}
		String returnString = new String(c);
		return returnString;
	}
}