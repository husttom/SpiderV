package com.yzq.os.spider.v.service.spider;

import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 前置处理抽象类
 * 
 * @author DEV-MARTIN
 * 
 */
public abstract class AbstractBeforeCrawlProcessor implements BeforeCrawlProcessor {

	/**
	 * HTTP访问服务类
	 */
	protected HttpClientService httpClientService;

	@Override
	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

}
