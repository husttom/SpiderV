package com.yzq.os.spider.v.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yzq.os.spider.v.service.inter.OuterSystemService;

/**
 * 外部系统接口控制器
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
@Controller
@RequestMapping("/etl")
public class OuterSystemController {
	
	@Autowired
	private OuterSystemService outerSystemService;

	/**
	 * 清空网站列表缓存数据
	 * @return
	 */
	@RequestMapping("clear_websites")
	@ResponseBody
	public String clear() {
		outerSystemService.clearWebsites();
		return "OK";
	}
}
